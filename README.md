<h1 align="center">yasift - Yet Another System Information Fetching Tool</h1> 

![Badge](https://honey.badgers.space/badge/LICENSE/AGPL%20v3/blue?cache=86400&) ![Badge](https://badgers.space/codeberg/stars/pwnkin/yasift) ![Badge](https://badgers.space/codeberg/issues/pwnkin/yasift) ![Badge](https://badgers.space/codeberg/release/pwnkin/yasift) 

## Overview

**yasift** is a simple implementation of tools like neofetch written in Python. It fetches system information such as OS details, hostname, kernel version, shell, terminal, uptime, CPU information, and memory usage, and displays them in a user-friendly format.

<img src="assets/yasift_in_use.png">


## Features

- Fetches various system information
- Displays the information with ASCII art (optional)
- Written in Python for easy customization and extension

## Installation

1. Clone the repository:

```bash
git clone https://codeberg.org/pwnkin/yasift.git
cd yasift
pip install -r requirements.txt
```

## Usage

using alias:
```bash
echo "alias yasift='python3 /location/of/program/'" >> ~/.bashrc && source ~/.bashrc
yasift
```

standalone:
```bash
chmod +x yasift
mv yasift /usr/local/bin/
```

### Flags
display help text:
```bash
yasift --help, -h
```

display version info:
```bash
yasift --version,-v
```

display without ascii art:
```bash
yasift --no-ascii, -n
```

display with execution time output:
```bash
yasift --time, -t
```


## License

[AGPL v3](https://choosealicense.com/licenses/agpl-3.0/)

